# day4a.py

# --- Day 4: Passport Processing ---
# You arrive at the airport only to realize that you grabbed your North Pole Credentials instead of your passport. While these documents are extremely similar, North Pole Credentials aren't issued by a country and therefore aren't actually valid documentation for travel in most of the world.
#
# It seems like you're not the only one having problems, though; a very long line has formed for the automatic passport scanners, and the delay could upset your travel itinerary.
#
# Due to some questionable network security, you realize you might be able to solve both of these problems at the same time.
#
# The automatic passport scanners are slow because they're having trouble detecting which passports have all required fields. The expected fields are as follows:
#
# byr (Birth Year)
# iyr (Issue Year)
# eyr (Expiration Year)
# hgt (Height)
# hcl (Hair Color)
# ecl (Eye Color)
# pid (Passport ID)
# cid (Country ID)
# Passport data is validated in batch files (your puzzle input). Each passport is represented as a sequence of key:value pairs separated by spaces or newlines. Passports are separated by blank lines.
#
# Here is an example batch file containing four passports:
#
# ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
# byr:1937 iyr:2017 cid:147 hgt:183cm
#
# iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
# hcl:#cfa07d byr:1929
#
# hcl:#ae17e1 iyr:2013
# eyr:2024
# ecl:brn pid:760753108 byr:1931
# hgt:179cm
#
# hcl:#cfa07d eyr:2025 pid:166559648
# iyr:2011 ecl:brn hgt:59in
# The first passport is valid - all eight fields are present. The second passport is invalid - it is missing hgt (the Height field).
#
# The third passport is interesting; the only missing field is cid, so it looks like data from North Pole Credentials, not a passport at all! Surely, nobody would mind if you made the system temporarily ignore missing cid fields. Treat this "passport" as valid.
#
# The fourth passport is missing two fields, cid and byr. Missing cid is fine, but missing any other field is not, so this passport is invalid.
#
# According to the above rules, your improved system would report 2 valid passports.
#
# Count the number of valid passports - those that have all required fields. Treat cid as optional. In your batch file, how many passports are valid?
#
# To begin, get your puzzle input.
import logging
logging.basicConfig(filename='day4a.log', encoding='utf-8', filemode='w', level=logging.DEBUG)

PuzzleInFileName = "day4a-input.txt"

def setup_logger(logger_name, log_file, level=logging.INFO):
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(message)s')
    fileHandler = logging.FileHandler(log_file, mode='w')
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(fileHandler)
    l.addHandler(streamHandler)

setup_logger('valid', "day4a-valid.txt")
setup_logger('rejected', "day4a-rejected.txt")
logger_v = logging.getLogger('valid')
logger_r = logging.getLogger('rejected')

# how to ues the logger
# logger_v.info('111messasage 1')
# logger_r.info('222ersaror foo')

def isValidPassport( PP ):

    RequiredFields = {
        'byr': 'Birth Year',
        'iyr': 'Issue Year',
        'eyr': 'Expiration Year',
        'hgt': 'Height',
        'hcl': 'Hair Color',
        'ecl': 'Eye Color',
        'pid': 'Passport ID'
        #'cid': 'Country ID'
    }

    for RF in RequiredFields:
        if RF not in PP.keys():
            return False

    return True # or false

def main():
    InFile = open( PuzzleInFileName , "r" )
    Lines = InFile.readlines()
    Passports = {}
    Passport = {}
    PassportStr = ""
    Answer = 0
    Pc = 0  # counter of total passports

    for Line in Lines:

        if Line.strip() != '':    # add lines to the passport until we find a blank
            # add the field to the Passport
            Fields = Line.split(" ")

            for F in Fields:
                k = str( F.split(":")[0].strip() )
                v = str( F.split(":")[1].strip() )
                Passport[k] = v
        else:
            # end of the passport, test it, add to results
            Pc += 1
            if isValidPassport( Passport ):
                #Answer += 1
                Answer = Answer + 1
                print "Valid(" + str( Answer ) + "):"
                print "Passport(" + str( len(Passports) ) + "):"
                print Passport
                print ""

            else:
                print "Not Valid:"

            # collection of passports we found
            Passports[ Pc ] = Passport
            # reset now that we're done with it
            Passport = {}

    print ""
    print "Passports checked: " + str( len(Passports) ) + " or:" + str(Pc)
    print "Answer: " + str( Answer )

    return 0


if __name__ == '__main__':
    main()
